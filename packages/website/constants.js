/* eslint-disable import/prefer-default-export */

export const MOBILE_QUERY = '(max-width: 779px)';
export const DESKTOP_QUERY = '(min-width: 1200px)';
export const NO_FOOTER_COMPONENT = ['navigation'];
export const NEW_WEBSITE_LOCATION = 'https://ak-mk-2-prod.netlify.com';
export const GOOGLE_ANALYTICS_ID = 'UA-6032469-95';
