import styled from 'styled-components';
import { colors, gridSize } from '@atlaskit/theme';

// eslint-disable-next-line
export const CodeBlock = styled.pre`
  background-color: ${colors.DN50};
  border-radius: 5px;
  margin: 14px 0;
  padding: ${gridSize}px;
`;
