import ActivityResource from './api/ActivityResource';

export * from './types';

export {
  ActivityResource
};
