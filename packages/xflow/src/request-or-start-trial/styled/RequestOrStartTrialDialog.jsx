import styled from 'styled-components';

const RequestOrStartTrialDialog = styled.div`
  text-align: left;
`;

RequestOrStartTrialDialog.displayName = 'RequestOrStartTrialDialog';
export default RequestOrStartTrialDialog;
