import styled from 'styled-components';

const RequestTrialFooter = styled.div`
  text-align: right;
  padding: 14px 20px 20px;
`;

RequestTrialFooter.displayName = 'RequestTrialFooter';
export default RequestTrialFooter;
