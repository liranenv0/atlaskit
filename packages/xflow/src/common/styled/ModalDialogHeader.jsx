import styled from 'styled-components';

const ModalDialogHeader = styled.div`
  padding: 20px 20px 14px;
`;

ModalDialogHeader.displayName = 'ModalDialogHeader';
export default ModalDialogHeader;
